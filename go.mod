module gitlab.com/johndelavega/testmod

require (
	gitlab.com/johndelavega/addpkg v0.1.0
	gitlab.com/johndelavega/go-modules v0.1.0
	gitlab.com/johndelavega/mathlib v0.3.0
	gitlab.com/johndelavega/mathpkg v0.1.0
)
