package main


import (
	"fmt"

	// mathlib1 "gitlab.com/johndelavega/go-mod" // Add() Version()
	// "gitlab.com/johndelavega/go-mod" // mathlib1 Add() Version()
	// renamed_package "gitlab.com/johndelavega/mathlib"
	mathlib "gitlab.com/johndelavega/mathlib"
	// mathlib "gitlab.com/johndelavega/go-modules" // mathlib Add() Version() ModInfo()
	// "./lib2" // -> cannot find module for path; OK if go.mod does not exist; used: go get -u custom-module

	"gitlab.com/johndelavega/addpkg"
	mathlib1 "gitlab.com/johndelavega/go-modules" // mathlib Add() Version() ModInfo()
	"gitlab.com/johndelavega/mathpkg"
)

const mVersion = "v0.2.3"

func main() {

	fmt.Printf("testmod %s (main app)\n\n", mVersion)

	a := mathlib.Add(2, 1)
	// a := renamed_package.Add(2, 1)
	b := mathlib1.Add(2, 2)
	// c := go_mod.Add(2, 2)
	c := addpkg.Add(3, 3)
	d := mathpkg.Add(4, 3)
	// e := lib2x.Add(4, 3)

	// fmt.Printf("Add: %s\n", strconv.Itoa(mathlib.Add(1, 2)))
	// fmt.Printf("Add: %s\n", strconv.Itoa(a))
	// fmt.Printf("mathlib Add: %d\n", a)
	fmt.Printf("mathlib Add: %d\n", a)
	fmt.Printf("mathlib1,go-modules, Add: %d\n", b)
	fmt.Printf("addpkg, Add(): %d\n", c)
	fmt.Printf("mathpkg, Add(): %d\n", d)
	// fmt.Printf("lib2x, import \"./lib2\"  Add(): %d\n", e)
	// a := strconv.Itoa(1)

	// fmt.Printf("Note: %s\n", mathlib.Note())
	fmt.Printf("mathlib1, go-modules,  Version: %s\n", mathlib1.Version())

	// fmt.Printf("ModInfo: %s\n", mathlib.ModInfo())
	fmt.Printf("renamed to mathlib1, ModInfo: %s\n", mathlib1.ModInfo())
	fmt.Printf("mathlib, ModInfo: %s\n", mathlib.ModInfo())
	fmt.Printf("addpkg, PackageInfo: %s\n", addpkg.PackageInfo())
	fmt.Printf("mathpkg, PackageInfo: %s\n", mathpkg.PackageInfo())

	// fmt.Printf("add: %s\n", a)
}
