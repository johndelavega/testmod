package lib2x

import "fmt"

const mVersion = "v0.1.0"

// Add two integer numbers
func Add(a int, b int) int {

	return a + b
}

// PackageInfo returns info string
func PackageInfo() string {

	return fmt.Sprintf("lib2 Version %s | lib2", mVersion)

}

// Version returns version string for debugging
func Version() string {

	return fmt.Sprintf("%s", mVersion)

}
